from __future__ import annotations

import argparse
import datetime
import getpass
import json
import os
import time
from dataclasses import dataclass
from pathlib import Path

import requests_oauthlib
from requests.structures import CaseInsensitiveDict
from requests.utils import quote

GITLAB_URL = os.environ.get("CI_SERVER_HOST", "gitlab.com")
COMMITTER_NAMES = json.loads(
    os.environ.get("CI_EBERICHTSHELPER_COMMITTER_NAMES", '["Christopher Mann"]')
)
PROJECT_ID = os.environ.get("CI_PROJECT_ID", "38548039")
GITLAB_PROJECT_TOKEN = os.environ.get("EBERICHTSHELPER_PROJECT_ACCESS_TOKEN")
GITLAB_PERSONAL_READONLY_TOKEN = os.environ.get("EBERICHTSHELPER_PERSONAL_READONLY_TOKEN")


@dataclass
class CommitData:
    message: str
    timestamp: time.struct_time


def main():
    token = (
        GITLAB_PERSONAL_READONLY_TOKEN
        if GITLAB_PERSONAL_READONLY_TOKEN
        else getpass.getpass("GitLab personal access-token (read access): ")
    )

    session = requests_oauthlib.OAuth2Session(token={"access_token": token})

    markdown = get_last_weeks_report(session)

    # GitLab Wiki
    title = quote(
        f"Week Report ({datetime.datetime.now()})"
    )  # make that stuff url safe, e.g. & to %26
    content = quote(markdown)

    wiki_data = f"format=markdown&title={title}&content={content}"

    headers = CaseInsensitiveDict()
    headers["Content-Type"] = "application/x-www-form-urlencoded"

    token = GITLAB_PROJECT_TOKEN or getpass.getpass("GitLab token (project write access): ")

    session = requests_oauthlib.OAuth2Session(token={"access_token": token})

    res = session.post(
        f"https://{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/wikis",
        data=wiki_data,
        headers=headers,
    )
    res.raise_for_status()
    print(res.json())


def main_files():
    token = (
        GITLAB_PERSONAL_READONLY_TOKEN
        if GITLAB_PERSONAL_READONLY_TOKEN
        else getpass.getpass("GitLab personal access-token (read access): ")
    )

    session = requests_oauthlib.OAuth2Session(token={"access_token": token})

    folder = Path("weekly_reports")
    folder.mkdir(exist_ok=True)
    for week_counter in range(15):
        markdown = get_last_weeks_report(
            session, date=datetime.date.today() - datetime.timedelta(days=7 * week_counter)
        )

        # GitLab Wiki
        filename = f"report_now-{week_counter}weeks.md"
        (folder / filename).write_text(markdown)


def get_last_weeks_report(
    session: requests_oauthlib.OAuth2Session,
    date: datetime.datetime = datetime.date.today(),
):
    date = datetime.datetime.combine(date, datetime.datetime.min.time())

    def last_seven_weekdays(last_date: datetime.datetime):
        """last seven days including the date itself."""
        for counter in range(7):
            yield last_date - datetime.timedelta(days=(6 - counter))

    return "\n\n".join([get_section_for_date(session, day) for day in last_seven_weekdays(date)])


def get_section_for_date(
    session: requests_oauthlib.OAuth2Session,
    date: datetime.datetime,
) -> str:
    prev_date, next_date = prev_and_next_day(date)
    prev_date_formatted = format_date_events_api(prev_date)
    next_date_formatted = format_date_events_api(next_date)

    res = session.get(
        f"https://{GITLAB_URL}/api/v4/events"
        + f"?before={next_date_formatted}&after={prev_date_formatted}"
    )
    res.raise_for_status()
    projects = set()
    for proj in res.json():
        projects.add(proj["project_id"])

    date_formatted = datetime.datetime.isoformat(date)
    next_date_formatted = datetime.datetime.isoformat(next_date)
    data: dict[str, list[CommitData]] = {}
    for proj in projects:
        commits = session.get(
            f"https://{GITLAB_URL}/api/v4/projects/{proj}/repository/commits"
            + f"?since={date_formatted}&until={next_date_formatted}&all=true"  # midnight to midnight, and all branches
        )
        commits.raise_for_status()
        data[proj] = []
        for commit in commits.json():
            if commit["committer_name"] in COMMITTER_NAMES:
                data[proj].append(
                    CommitData(
                        commit["message"],
                        time.strptime(commit["created_at"], "%Y-%m-%dT%H:%M:%S.%f%z"),
                    )
                )

    data = {
        proj: sorted(commits, key=lambda commit: commit.timestamp)
        for proj, commits in data.items()
    }

    projects_meta: dict[tuple] = {}
    for proj in projects:
        proj_meta = session.get(f"https://{GITLAB_URL}/api/v4/projects/{proj}")
        proj_meta.raise_for_status()
        proj_meta = proj_meta.json()
        projects_meta[proj] = (proj_meta["name"], proj_meta.get("description"))

    weekdays = {
        1: "Monday",
        2: "Tuesday",
        3: "Wednesday",
        4: "Thursday",
        5: "Friday",
        6: "Saturday",
        7: "Sunday",
    }
    weekday = weekdays[date.isoweekday()]

    section_header = f"# **{weekday}: {format_date_events_api(date)}**\n"
    section_body = []
    for proj, commits in data.items():
        name = projects_meta.get(proj, ("???", "-"))[0] or "???"
        description = projects_meta.get(proj, ("???", "-"))[1] or "..."
        section_body.append(f"## {name}\n\n*Description*:\n{description}\n")

        commit_list = []
        for commit in commits:
            message = commit.message
            timestamp = time.strftime("%Y-%m-%dT%H:%M:%S", commit.timestamp)
            commit_list.append(f"- @{timestamp}: {message}")
        section_body.append("\n".join(commit_list))

    return "\n".join([section_header, *section_body])


def prev_and_next_day(date: datetime.datetime):
    return (date - datetime.timedelta(days=1), date + datetime.timedelta(days=1))


def format_date_events_api(date: datetime.datetime):
    return datetime.datetime.strftime(date, "%Y-%m-%d")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--files", action="store_true")
    args = parser.parse_args()
    if not args.files:
        main()
    else:
        main_files()
