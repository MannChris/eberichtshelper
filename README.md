# eBerichtshelper

The eBerichtshelper scrapes the GitLab-API to give you a summary what you worked on every day of the past week.
On top of potentially saving you~~r ass~~ time, it will also engrain some good behavior; you have to adhere to best practice:
- commit (and push) often
- write meaningful commit messages

## How to properly "fork" this repository

Create an empty project on GitLab.
Then clone that empty projekt and inside the empty repo sitory execute:
```bash
# Add this project as a remote repository (now named upstream).
git remote add upstream https://gitlab.com/MannChris/eberichtshelper.git

# Pull the current main branch of this repository.
# Just do this whenever you want to sync your "fork" with the upstream repository.
git pull upstream main

# Push the new changes to your project.
git push origin main

```

Note: Do not mirror an external repository on your company GitLab instance if you plan on taking advantage of GitLab-CI. 

## Usage with GitLab-CI:

Set the following CI-Variables:

- CI_EBERICHTSHELPER_COMMITTER_NAMES: Names you commit under as json array
- EBERICHTSHELPER_PROJECT_ACCESS_TOKEN: Project access token with write access (api)
- EBERICHTSHELPER_PERSONAL_READONLY_TOKEN: Readonly personal access token
- CI_EBERICHTSHELPER_IMAGE: Docker image to run the scropt in, e.g. python:3.9-alpine
- CI_EBERICHTSHELPER_PIP_INDEX_URL: URL of your pypi

Then you can trigger (or schedule) the pipeline with the following environment variable set, to run the script:

- CI_EBERICHTSHELPER_EXEC: TRUE

## Pro tip

Keep track of your other (non coding) ToDos in a repository.
